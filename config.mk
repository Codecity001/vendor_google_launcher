# Nexus Launcher
PRODUCT_SOONG_NAMESPACES += \
    vendor/google/launcher

PRODUCT_COPY_FILES += \
    vendor/google/launcher/product/etc/default-permissions/saitama.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/default-permissions/saitama.xml \
    vendor/google/launcher/product/etc/permissions/privapp-permissions-com.google.android.apps.nexuslauncher.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-com.google.android.apps.nexuslauncher.xml \
    vendor/google/launcher/product/etc/permissions/privapp-permissions-com.google.android.as.oss.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-com.google.android.as.oss.xml \
    vendor/google/launcher/product/etc/permissions/privapp-permissions-com.google.android.as.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-com.google.android.as.xml \
    vendor/google/launcher/product/etc/preferred-apps/google.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/preferred-apps/google.xml \
    vendor/google/launcher/product/etc/sysconfig/game_overlay.xml:$(TARGET_COPY_OUT_PRODUCT)etc/sysconfig/game_overlay.xml \
    vendor/google/launcher/product/etc/sysconfig/google_build.xml:$(TARGET_COPY_OUT_PRODUCT)etc/sysconfig/google_build.xml \
    vendor/google/launcher/product/etc/sysconfig/google-hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_PRODUCT)etc/sysconfig/google-hiddenapi-package-whitelist.xml \
    vendor/google/launcher/product/etc/sysconfig/google-staged-installer-whitelist.xml:$(TARGET_COPY_OUT_PRODUCT)etc/sysconfig/google-staged-installer-whitelist.xml \
    vendor/google/launcher/product/etc/sysconfig/google.xml:$(TARGET_COPY_OUT_PRODUCT)etc/sysconfig/google.xml \
    vendor/google/launcher/product/etc/sysconfig/nexus.xml:$(TARGET_COPY_OUT_PRODUCT)etc/sysconfig/nexus.xml \
    vendor/google/launcher/product/etc/sysconfig/nga.xml:$(TARGET_COPY_OUT_PRODUCT)etc/sysconfig/nga.xml \
    vendor/google/launcher/product/etc/sysconfig/pixel-launcher-hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel-launcher-hiddenapi-package-whitelist.xml \
    vendor/google/launcher/product/etc/sysconfig/preinstalled-packages-platform-handheld-product.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/preinstalled-packages-platform-handheld-product.xml \
    vendor/google/launcher/product/etc/sysconfig/preinstalled-packages-product-pixel-2017-and-newer.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/preinstalled-packages-product-pixel-2017-and-newer.xml \
    vendor/google/launcher/system_ext/etc/permissions/privapp-permissions-com.google.android.apps.wallpaper.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-com.google.android.apps.wallpaper.xml \
    vendor/google/launcher/bin/aosp_enhancer32:$(TARGET_COPY_OUT_SYSTEM)/bin/aosp_enhancer32 \
    vendor/google/launcher/bin/aosp_enhancer64:$(TARGET_COPY_OUT_SYSTEM)/bin/aosp_enhancer64

PRODUCT_PACKAGES += \
    ThemedIconsOverlay \
    PixelRecentsProvider\
    NexusLauncherRelease \
    NexusLauncherOverlay \
    WallpaperPickerGoogleRelease